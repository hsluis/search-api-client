<?php
namespace Hansvandersluis\SearchApi;

use \Cache;
use \Log;

class SearchClient {

    /**
     * Config defaults
     *
     * @var array
     */
    private $config = [

        // Service URL
        'serviceBaseUrl' => '',

        // API logging
        'enableLog' => false,

        // Cache life time in minutes
        'cacheLifetime' => '',

        // Credentials
        'email' => '',
        'password' => ''
    ];

    /**
     * SearchClient constructor.
     * @param array $options
     */
    public function __construct($options = []){

        // Set the config
        foreach($options as $key => $value){
            $this->config[$key] = $value;
        }
    }

    /**
     * Search the term with the api
     *
     * @param $term
     * @return string
     */
    public function search($term){
        $parameters = [
            'term' => $term
        ];
        $searchResult= $this->callEndPoint('search', $parameters, [], 'api_search_result');

        return $searchResult;
    }

    /**
     * Send user feedback to the api
     *
     * @param $id
     * @return string
     */
    public function feedback($id){
        $parameters = [
            'feedback' => $id
        ];
        $result= $this->callEndPoint('feedback', $parameters, [], 'api_feedback_result');
        return $result;
    }

    /**
     * Update an item
     *
     * @param $id
     * @param $data
     * @return string
     */
    public function update($id, $postData){
        $parameters= [
            'id' => $id
        ];
        $result= $this->callEndPoint('update', $parameters, $postData, 'api_update_result');
        return $result;
    }

    /**
     * Update the entire index
     *
     * @param $index
     * @param $type
     * @param $data
     * @return string
     */
    public function updateAll($index, $type, $postData){
        $parameters= [
            'index' => $index,
            'type' => $type
        ];
        $result= $this->callEndPoint('update-all', $parameters, $postData, 'api_update_all_result');
        return $result;
    }

    /**
     * Issue the call to the endpoint, check the cache for existing data,
     * , check the request authentication, and also resets the authentication
     * status if needed
     *
     * @param string $endpoint endpoint name
     * @param array $parameters endpoint parameters
     * @param string $cachename name of the local cache for the endpoint result
     * @return string
     */
    public function callEndPoint($endpoint, $parameters, $postData, $cachename){

        // Declare variables
        $result=[];

        // Fetch the data from the cache first
//        if(empty($postData) && Cache::has($cachename)){
//            $content= Cache::get($cachename);
//            $result= json_decode($content,true);
//            $this->log('fetched search result from cache');
//        }
//        else{

            // When token is on cache
            if(Cache::has('api_token')){
                $result= $this->getEndpointResult($endpoint, $parameters, $postData);

                // When the result says "token invalid", reset api_token on cache and call the endpoint again
                if($result == 'token_invalid'){
                    Cache::forget('api_token');
                    $result= $this->callEndPoint($endpoint, $parameters, $postData, $cachename);
                }
                else{
                    // Save result to cache when GET method
                    if(empty($postData)) {
                        $this->log('Saving search result to cache');
                        Cache::put($cachename, json_encode($result), $this->config['cacheLifetime']);
                    }
                }
            }
            else{
                // Authenticate when token doesn't exist
                if($this->authenticate()){

                    // Try to call the endpoint again with a new token
                    $result= $this->callEndPoint($endpoint, $parameters, $postData, $cachename);
                }
                else{
                    // Authentication is not posible
                    $result="AUTHENTICATION_ERROR";
                }
            }
//        }
        return $result;
    }

    /**
     * Connect to the api endpoint and get the data
     *
     * @param string $endpoint endpoint name
     * @param array $parameters endpoint parameters
     * @param string $cachename name of the local cache for the endpoint result
     *
     * @return string the enpoint content or a string when the token is invalid
     */
    public function getEndpointResult($endpoint, $parameters, $postData){

        // Combine the endpoint and parameters to make up the url
        $endpointParameters = '';
        foreach ($parameters as $paramKey => $paramData) {
            $endpointParameters .= '/' . $paramData;
        }

        // Connect
        $result= $this->connect($endpoint.$endpointParameters, $postData);

        // Convert the result to an array
        $endpointResult= json_decode($result['response'],true);

        // Log token errors
        if($endpointResult['response'] == 'token_invalid'){
            $endpointResult= 'token_invalid';
            $this->log('searching with invalid token');
        }
        else if($endpointResult['response'] == 'token_expired'){
            $endpointResult= 'token_invalid';
            $this->log('searching with invalid token');
        }
        else if($endpointResult['response'] == 'token_absent'){
            $endpointResult= 'token_invalid';
            $this->log('searching with invalid token');
        }
        else{
            $this->log('Problems searching the api');
        }

        // Return the result
        return $endpointResult;
    }

    /**
     * Connect to the webservice and retrieve the result
     *
     * @param type $resource
     * @param type $method http method
     */
    public function connect($resource, $postData=null){

        // Set method
        $method = !empty($postData) ? 'post' : 'get';

        // Get api token from cache
        $apiToken= Cache::get('api_token');

        // Set resource's full url
        $url= $this->config['serviceBaseUrl'].$resource;

        // Add parameters to url on get method
        //        if($method == 'get'){
        //            $url.='?';
        //            foreach($parameters as $paramKey => $paramVal){
        //                $url.= $paramKey. '=' .$paramVal.'&';
        //            }
        //            $url= substr($url, 0, strlen($url)-1);
        //        }

        // Set headers
        $headers=['User-Agent: curl/7.41.0'];
        if($method == 'post'){
            $headers[]= 'Content-Type: application/x-www-form-urlencoded';
        }
        else{
            $headers[]='Content-Type: text/plain; charset=UTF-8';
        }

        if(isset($apiToken) && $apiToken != ''){
            $headers[]= 'Authorization: Bearer '.$apiToken;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if($method == 'post'){
            curl_setopt($ch, CURLOPT_POST, true);
            if(isset($postData)){
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
            }
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        //curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $result['response'] = curl_exec($ch);
        $result['status']= curl_getinfo($ch, CURLINFO_HTTP_CODE);

        //Log::info(curl_getinfo($ch));

        if($result['status'] != 200){
            $this->log('Api call:' . $result['response']);
        }

        $curlError= curl_error($ch);

        if($curlError != ''){
            $this->log('CURL_ERRORS: '. $curlError);
        }
        curl_close($ch);

        return $result;
    }


    /**
     * authenticate with the api and save the token to cache
     */
    public function authenticate(){

        $result=false;

        // Set the post fields using the config values
        $postData= [
            'email' => $this->config['email'],
            'password' => $this->config['password'],
        ];

        // Send authentication request
        $response= $this->connect('auth',$postData);
        $response= json_decode($response['response']);

        // When the authentication is correct the token is received
        if(isset($response->token)){
            $this->log('Authentication - success');

            // Save to api_token on cache
            Cache::put('api_token', $response->token, $this->config['cacheLifetime']);
            $this->log('Authentication - token saved on cache');
            $result=true;
        }
        else{

            // Reset api key
            Cache::forget('api_token');
            $this->log('Authentication - failed : token removed from cache');
        }

        return $result;
    }

    /**
     * Write out a log message
     *
     * @param $message
     */
    public function log($message){
        if($this->config['enableLog']){
            Log::info('SEARCH API: '. $message);
        }
    }
}